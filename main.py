from flask import render_template, request, Flask

import sql

app = Flask(__name__)


@app.route("/")
def hello_world():
    app.logger.info('Index page refreshed')
    return render_template('main.html')


@app.route("/health")
def health():
    app.logger.info('App is healthy')
    return "ok"


@app.route("/", methods=["POST"])
def form_post():
    if request.method == "POST":
        username = request.form.get("fname")
        age = request.form.get("fage")
        sql.insert_user(username, age)
    app.logger.info('%s with age %s added successfully', username, age)
    return render_template("main.html")


@app.route("/get-all-users")
def get_all_users():
    app.logger.info('Listed all users')
    users = sql.get_all_users()
    lines = '<h1>All users:</h1>'
    for user in users:
        username = user[1]
        age = user[2]
        line = f"<li>{username}, age - {age} <li>"
        lines += line
    return lines
