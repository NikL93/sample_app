import psycopg2
import os

# CREATE DATABASE main;
# USE main;
# CREATE TABLE users(user_id SERIAL PRIMARY KEY,
# username VARCHAR(255) NOT NULL, age INT NOT NULL)

# Database connection parameters
db_params = {
    'host': os.getenv("DATABASE_HOST"),
    'dbname': os.getenv("DATABASE"),
    'user': os.getenv("POSTGRES_USER"),
    'password': os.getenv("POSTGRES_PASSWORD"),
    'port': os.getenv("DATABASE_PORT")
}


def insert_user(username: str, age: int):
    try:
        connection = psycopg2.connect(**db_params)
        connection.autocommit = True
        cursor = connection.cursor()
        sql_query = f"INSERT INTO users (username, age) " \
                    f"VALUES ('{username}', {age});"
        print(sql_query)
        cursor.execute(sql_query)
        cursor.close()
        connection.close()
    except psycopg2.Error as error:
        print("Error while connecting to PostgreSQL:", error)


def get_all_users():
    try:
        connection = psycopg2.connect(**db_params)
        cursor = connection.cursor()
        sql_query = "SELECT * FROM users;"
        cursor.execute(sql_query)
        users = cursor.fetchall()
        for user in users:
            print(user)
        cursor.close()
        connection.close()
        return users

    except psycopg2.Error as error:
        print("Error while connecting to PostgreSQL:", error)
