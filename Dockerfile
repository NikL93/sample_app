FROM python:3.7-alpine

RUN mkdir /sample-app
COPY . /sample-app
WORKDIR /sample-app
RUN pip3 install --no-cache-dir -r requirements.txt
EXPOSE 80

CMD ["python", "main.py"]